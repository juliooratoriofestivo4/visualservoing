from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics

from controlvisual.visual_servoing import control_visual

import numpy as np


file_name = "/home/huro/Escritorio/Robotica/Ejemplo_KDL/catkin_ws/src/iai_ur_description/urdf/ur3.urdf"

def robot_from_urdf():

	#abrimos el fichero urdf.
	f = file(file_name)
	
	#creamos el robot a partir del URDF.
	robot = URDF.from_xml_string(f.read())

	#guardamos el link de la base y del extremo.
	base_link = robot.get_root()
	end_link = robot.link_map.keys()[len(robot.link_map)-1]

	return robot,base_link,end_link


if __name__ == '__main__':

	
	robot,base_link,end_link = robot_from_urdf()

	#creamos un objeto de clase KDLKINETMATICS.
	kdl_kin = KDLKinematics(robot, base_link, end_link)

	#Creamos posiciones.
	#q = kdl_kin.random_joint_angles()
	q = np.array([3.14,0,1.57,0,1.57,0])
	print("Position: ",q)

	#Calculamos Jacobiano directo e inverso.
	J = kdl_kin.jacobian(q)
	J_inv = np.linalg.pinv(J)

	print ('J:', J)
	print('J inverse',J_inv)

	

	
