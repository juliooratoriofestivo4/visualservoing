import numpy as np 


#uv = [x1,y1,x2,y2...
def control_visual(uv,uv_d,n,fx,fy,cx,cy,l):

    s = []
    s_d = []
    z = 1
    pseudo_jacobian = []
    np_jacobian = np.zeros((2 * n, 6))

    '''
    Primer paso: convertimos las caracteristicas de pixeles a metros. 
    Para ello utilizamos la siguiente formula:

        x = (xp - cx) / fx ; y = (yp - cy) / fy

        donde:
        - x,y son las caracteristicas en metros
        - cx,cy es el centro optico de la imagen
        - fx, fy es la distancia focal
    '''

    for i in range(len(uv)):

        #0,2,4,6... son las x's.
        if i % 2 == 0:
            aux_x = (uv[i] - cx) / fx
            s.append(aux_x) 
        #1,3,5... son las y's.
        else:
            aux_y = (uv[i] - cy) / fy
            s.append(aux_y)

    for i in range(len(uv_d)):

        #0,2,4,6... son las x's.
        if i % 2 == 0:
            aux_x = (uv_d[i] - cx) / fx
            s_d.append(aux_x) 
        #1,3,5... son las y's.
        else:
            aux_y = (uv[i] - cy) / fy
            s_d.append(aux_y)

    np_s = np.array(s)
    np_s_d = np.array(s_d)
    print(np_s)
    print(np_s.shape)
    print(np_s_d)
    print(np_s_d.shape)

    '''
    Segundo paso: calculamos la matriz jacobiana para cada caracteristica.
    '''
    for i in range(len(s)):
        
        if i % 2 == 0:
            np_jacobian[i][0] = (-1 / z)
            np_jacobian[i][1] = 0
            np_jacobian[i][2] = (s[i] / z)
            np_jacobian[i][3] = (s[i] * s[i+1])
            np_jacobian[i][4] = (- (1 + s[i] * s[i]))
            np_jacobian[i][5] = (s[i+1])

        else:
            np_jacobian[i][0] = (0)
            np_jacobian[i][1] = (- (1 / z))
            np_jacobian[i][2] = (s[i] / z)
            np_jacobian[i][3] = (1 + s[i] * s[i])
            np_jacobian[i][4] = (-s[i-1] * s[i])
            np_jacobian[i][5] = (-s[i-1])
    
    print(np_jacobian)
    print(np_jacobian.shape)

    '''
    Tercer paso: con np.linalg.pinv calculamos la matriz pseudo inversa de Moore-Penrose.
    '''
    pseudo_jacobian = np.linalg.pinv(np_jacobian)

    print(pseudo_jacobian)
    print(pseudo_jacobian.shape)

    '''
    Cuarto paso: calculamos la ley de control con la que obtenemos la velocidad 
    que le pasamos al robot.
    '''
    cvc = -l * np.dot(pseudo_jacobian,(np_s - np_s_d))

    return cvc


