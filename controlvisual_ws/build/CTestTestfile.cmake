# CMake generated Testfile for 
# Source directory: /home/huro/Escritorio/Robotica/Proyecto/PROYECTO/controlvisual_ws/src
# Build directory: /home/huro/Escritorio/Robotica/Proyecto/PROYECTO/controlvisual_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(apriltag_lib)
subdirs(hrl-kdl/hrl_kdl)
subdirs(iai_ur_description)
subdirs(urdf_parser_py)
subdirs(hrl-kdl/hrl_geom)
subdirs(project)
subdirs(hrl-kdl/pykdl_utils)
