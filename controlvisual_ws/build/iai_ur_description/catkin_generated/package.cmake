set(_CATKIN_CURRENT_PACKAGE "iai_ur_description")
set(iai_ur_description_VERSION "1.1.5")
set(iai_ur_description_MAINTAINER "Alexander Bubeck <aub@ipa.fhg.de>")
set(iai_ur_description_PACKAGE_FORMAT "1")
set(iai_ur_description_BUILD_DEPENDS )
set(iai_ur_description_BUILD_EXPORT_DEPENDS "urdf")
set(iai_ur_description_BUILDTOOL_DEPENDS "catkin")
set(iai_ur_description_BUILDTOOL_EXPORT_DEPENDS )
set(iai_ur_description_EXEC_DEPENDS "urdf")
set(iai_ur_description_RUN_DEPENDS "urdf")
set(iai_ur_description_TEST_DEPENDS )
set(iai_ur_description_DOC_DEPENDS )
set(iai_ur_description_URL_WEBSITE "http://ros.org/wiki/iai_ur_description")
set(iai_ur_description_URL_BUGTRACKER "")
set(iai_ur_description_URL_REPOSITORY "")
set(iai_ur_description_DEPRECATED "")